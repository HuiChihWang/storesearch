//
//  UIImageView+DownloadImage.swift
//  StoreSearch
//
//  Created by Hui Chih Wang on 2021/1/5.
//

import UIKit

extension UIImageView {
    func loadImage(remoteURL: URL) -> URLSessionDownloadTask {
        let session = URLSession.shared
        
        let downloadTask = session.downloadTask(with: remoteURL) { [weak self] localURL, _, error in
            if error == nil, let url = localURL, let data = try? Data(contentsOf: url), let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    if let weakSelf = self {
                        weakSelf.image = image
                    }
                }
            }
        }
        
        downloadTask.resume()
        return downloadTask
    }
}
