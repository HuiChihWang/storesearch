//
//  LandscapeViewController.swift
//  StoreSearch
//
//  Created by Hui Chih Wang on 2021/1/7.
//

import UIKit

class LandscapeViewController: UIViewController {

    var search: Search!
    private var downloadTasks = [URLSessionDownloadTask]()
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var pageControl: UIPageControl!
    
    deinit {
        print("release lanscape view")
        downloadTasks.forEach { task in
            task.cancel()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Remove constraints from main view
        view.removeConstraints(view.constraints)
        view.translatesAutoresizingMaskIntoConstraints = true
        // Remove constraints for page control
        pageControl.removeConstraints(pageControl.constraints)
        pageControl.translatesAutoresizingMaskIntoConstraints = true
        // Remove constraints for scroll view
        scrollView.removeConstraints(scrollView.constraints)
        scrollView.translatesAutoresizingMaskIntoConstraints = true
        
        view.backgroundColor = UIColor(patternImage: UIImage(named: "LandscapeBackground")!)
        pageControl.numberOfPages = 0
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let safeFrame = view.safeAreaLayoutGuide.layoutFrame
        scrollView.frame = safeFrame
        pageControl.frame = CGRect(
            x: safeFrame.origin.x,
            y: safeFrame.size.height - pageControl.frame.size.height,
            width: safeFrame.size.width,
            height: pageControl.frame.size.height
        )
        
        if case .results(let list) = search.state {
            configureButtons(results: list)
        }
        
        switch search.state {
        case .loading:
            showSpinner()
        case .noResults:
            showNothingFoundLabel()
        case .results(let list):
            configureButtons(results: list)
        default:
            return
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetail" {
            if case .results(let list) = search.state {
                let detailViewController = segue.destination as! DetailViewController
                let searchResult = list[(sender as! UIButton).tag - 2000]
                detailViewController.searchResult = searchResult
            }
        }
    }
    
    // MARK: - Actions
    @IBAction func pageChanged(_ sender: UIPageControl) {
        UIView.animate(withDuration: 0.3,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations: {
                        self.scrollView.contentOffset = CGPoint(
                            x: self.scrollView.bounds.size.width * CGFloat(sender.currentPage),
                            y: 0)
                       },
                       completion: nil)
    }
}

extension LandscapeViewController {
    private func configureButtons(results: [SearchResult]) {
        let itemCount = results.count
        let buttonSize = CGSize(width: 82, height: 82)
        let buttonPadding = CGSize(width: 7, height: 3)
        let itemSize = 2 * buttonPadding + buttonSize
        let gridShape = calculateGridSize(itemSize: itemSize)
        let gridMargin = calculateGridMargin(gridShape: gridShape, itemSize: itemSize)
        let totalPadding = gridMargin + buttonPadding
        let grigSize = itemSize + 2 * gridMargin
        
        let screenSize = scrollView.bounds.size
        let itemsPerPage = Int(gridShape.area)
        let pageNumber = Int(ceil(Double(itemCount) / Double(itemsPerPage)))
        scrollView.contentSize = CGSize(width: CGFloat(pageNumber) * screenSize.width, height: screenSize.height)
        pageControl.numberOfPages = pageNumber
        
        for (index, result) in results.enumerated() {
            let button = UIButton(type: .custom)
            button.setBackgroundImage(UIImage(named: "LandscapeButton"), for: .normal)
            
            // position
            let pageIndex = index / itemsPerPage
            let indexOnPage = index % itemsPerPage
            let gridLocation = convertIndextoRowColumn(index: indexOnPage, gridSize: gridShape)
            let pageOriginX = CGFloat(pageIndex) * screenSize.width
            let orginal = CGPoint(
                x: gridLocation.x * grigSize.width + totalPadding.width + pageOriginX,
                y: gridLocation.y * grigSize.height + totalPadding.height
                )
            button.frame = CGRect(origin: orginal, size: buttonSize)
            
            // button content
            downLoadImage(for: result, addPlaceOn: button)
            button.tag = 2000 + index
            button.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
            // add button
            scrollView.addSubview(button)
            
        }
    }
    private func calculateGridSize(itemSize: CGSize) -> CGSize {
        let screenSize = scrollView.bounds.size
        let columnPerPage = Int(screenSize.width / itemSize.width)
        let rowPerPage = Int(screenSize.height / itemSize.height)
        
        return CGSize(width: columnPerPage, height: rowPerPage)
    }
    
    private func calculateGridMargin(gridShape: CGSize, itemSize: CGSize) -> CGSize {
        let screenSize = scrollView.bounds.size
        
        let marginX: CGFloat = (screenSize.width - CGFloat(gridShape.width) * itemSize.width) / (2 * gridShape.width)
        let marginY: CGFloat = (screenSize.height - CGFloat(gridShape.height) * itemSize.height) / (2 * gridShape.height)
        
        return CGSize(width: marginX, height: marginY)
    }
    
    private func convertIndextoRowColumn(index: Int, gridSize: CGSize) -> CGPoint {
        let row = index / Int(gridSize.width)
        let column = index % Int(gridSize.width)
        
        return CGPoint(x: column, y: row)
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "ShowDetail", sender: sender)
    }

    

}

// MARK: - Download Images to button
extension LandscapeViewController {
    func downLoadImage(for result: SearchResult, addPlaceOn button: UIButton) {
        if let url = result.imageSmallURL {
            let task = URLSession.shared.downloadTask(with: url) { [weak button] url, _, error in
                
                if error == nil, let localUrl = url, let data = try? Data(contentsOf: localUrl), let image = UIImage(data: data)  {
                    DispatchQueue.main.async {
                        if let button = button {
                            button.setImage(image, for: .normal)
                        }
                    }
                }
            }
            task.resume()
            downloadTasks.append(task)
        }
    }
}


extension LandscapeViewController {
    func searchResultsReceived() {
        // TODO: hide activity indicator not working
        hideSpinner()
        
        switch search.state {
        case .noResults:
            showNothingFoundLabel()
        case .results(let list):
            configureButtons(results: list)
        default:
            break
        }
    }
    
    private func showSpinner() {
        let spinner = UIActivityIndicatorView(style: .large)
        spinner.center = CGPoint(
            x: scrollView.bounds.midX,
            y: scrollView.bounds.midY)
        spinner.tag = 1001
        view.addSubview(spinner)
        spinner.startAnimating()
    }
    
    private func hideSpinner() {
        view.viewWithTag(1001)?.removeFromSuperview()
    }
    
    private func showNothingFoundLabel() {
        let label = UILabel(frame: CGRect.zero)
        label.text = NSLocalizedString("Nothing Found", comment: "Localized label: Nothing Found")
        label.textColor = UIColor.label
        label.backgroundColor = UIColor.clear
        
        label.sizeToFit()
        
        var rect = label.frame
        rect.size.width = ceil(rect.size.width / 2) * 2    // make even
        rect.size.height = ceil(rect.size.height / 2) * 2  // make even
        label.frame = rect
        
        label.center = CGPoint(
            x: scrollView.bounds.midX,
            y: scrollView.bounds.midY)
        view.addSubview(label)
    }
}
extension LandscapeViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let width = scrollView.bounds.size.width
        let pageIndex = Int(scrollView.contentOffset.x / width + 0.5)
        pageControl.currentPage = pageIndex
    }
}

extension CGSize: AdditiveArithmetic {
    public static func - (lhs: CGSize, rhs: CGSize) -> CGSize {
        return CGSize(width: lhs.width - rhs.width, height: lhs.height - rhs.height)
    }
    
    public static func + (lhs: CGSize, rhs: CGSize) -> CGSize {
        return CGSize(width: lhs.width + rhs.width, height: lhs.height + rhs.height)
    }
    
    public static func * (lhs: CGSize, rhs: CGFloat) -> CGSize {
        return CGSize(width: lhs.width * rhs, height: lhs.height * rhs)
    }
    
    public static func * (lhs: CGFloat, rhs: CGSize) -> CGSize {
        return CGSize(width: rhs.width * lhs, height: rhs.height * lhs)
    }
    
    public var area: CGFloat {
        width * height
    }
}
