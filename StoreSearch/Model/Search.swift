//
//  Search.swift
//  StoreSearch
//
//  Created by Hui Chih Wang on 2021/1/8.
//

import Foundation

typealias SearchComplete = (Bool) -> Void

class Search {
    enum State {
      case notSearchedYet
      case loading
      case noResults
      case results([SearchResult])
    }
    
    enum Category: Int {
        case all = 0
        case music = 1
        case software = 2
        case ebooks = 3
        
        var type: String {
            switch self {
            case .all: return ""
            case .music: return "musicTrack"
            case .software: return "software"
            case .ebooks: return "ebook"
            }
        }
    }

    private(set) var state: State = .notSearchedYet
    
//    private(set) var results: [SearchResult]?
//
//    var hasSearch: Bool {
//        results != nil
//    }
//
//    private(set) var isLoading = false
    
    private var currentTask: URLSessionTask?
    
    func performSearch(for searchText: String, with catogory: Category = .all, completion: @escaping SearchComplete) {
        if !searchText.isEmpty {
            currentTask?.cancel()
            state = .loading
            
            let requestURL = Search.iTunesURL(searchText: searchText, category: catogory)
            print("URL: '\(requestURL)'")
            
            let session = URLSession.shared
            currentTask = session.dataTask(with: requestURL) { data, response, error in
                
                var newState = State.notSearchedYet
                var isSuccess = false
                
                if let error = error as NSError?, error.code == -999 {
                    print("Failure! \(error.localizedDescription)")
                    return
                }
                if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let data = data {
                    let results = Search.parse(data: data).sorted(by: <)
                    newState = results.isEmpty ? .noResults : State.results(results)
                    
                    print("Success!")
                    isSuccess = true
                }
                
                DispatchQueue.main.async {
                    //TODO: figure out why state update here
                    self.state = newState
                    completion(isSuccess)
                }
            }
            currentTask?.resume()
        }
    }
    
    private static func iTunesURL(searchText: String, category: Category = .all) -> URL {
        let locale = Locale.autoupdatingCurrent
        let language = locale.identifier
        let countryCode = locale.regionCode ?? "en_US"
        
        let encodedText = searchText.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let urlString = "https://itunes.apple.com/search?" + "term=\(encodedText)&limit=200&entity=\(category.type)" + "&lang=\(language)&country=\(countryCode)"
        
        let url = URL(string: urlString)
        print("URL: \(url!)")
        return url!
    }
    
    private static func parse(data: Data) -> [SearchResult] {
        do {
            let decoder = JSONDecoder()
            let result = try decoder.decode(ResultArray.self, from: data)
            return result.results
        } catch {
            print("JSON Error: \(error)")
            return []
        }
    }
}
