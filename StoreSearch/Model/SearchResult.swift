//
//  SearchResult.swift
//  StoreSearch
//
//  Created by Hui Chih Wang on 2020/12/29.
//

import Foundation

private let typeForKind = [
    "album": NSLocalizedString(
        "Album",
        comment: "Localized kind: Album"),
    "audiobook": NSLocalizedString(
        "Audio Book",
        comment: "Localized kind: Audio Book"),
    "book": NSLocalizedString(
        "Book",
        comment: "Localized kind: Book"),
    "ebook": NSLocalizedString(
        "E-Book",
        comment: "Localized kind: E-Book"),
    "feature-movie": NSLocalizedString(
        "Movie",
        comment: "Localized kind: Feature Movie"),
    "music-video": NSLocalizedString(
        "Music Video",
        comment: "Localized kind: Music Video"),
    "podcast": NSLocalizedString(
        "Podcast",
        comment: "Localized kind: Podcast"),
    "software": NSLocalizedString(
        "App",
        comment: "Localized kind: Software"),
    "song": NSLocalizedString(
        "Song",
        comment: "Localized kind: Song"),
    "tv-episode": NSLocalizedString(
        "TV Episode",
        comment: "Localized kind: TV Episode")
]


class ResultArray: Codable {
    var resultCount: Int = 0
    var results: [SearchResult] = []
    
}

class SearchResult: Codable, CustomStringConvertible {
    
    
    enum CodingKeys: String, CodingKey {
        case imageSmall = "artworkUrl60"
        case imageLarge = "artworkUrl100"
        case itemGenre = "primaryGenreName"
        case bookGenre = "genres"
        case itemPrice = "price"
        case kind, artistName, currency
        case trackName, trackPrice, trackViewUrl
        case collectionName, collectionViewUrl, collectionPrice
    }
    
    
    var description: String {
        "\nResult - Kind: \(kind ?? "None"), Name: \(name), Artist Name: \(artistName ?? "None")"
    }
    
    var trackName: String? = ""
    var artistName: String? = ""
    var kind: String? = ""
    var trackPrice: Double? = 0.0
    var currency = ""
    
    var imageSmall: String? = ""
    var imageLarge: String? = ""
    
    var trackViewUrl: String?
    var collectionName: String?
    var collectionViewUrl: String?
    var collectionPrice: Double?
    var itemPrice: Double?
    var itemGenre: String?
    var bookGenre: [String]?
    
    
    
    var name: String {
        trackName ?? collectionName ?? ""
    }
    
    var storeURL: String {
        return trackViewUrl ?? collectionViewUrl ?? ""
    }
    
    var price: Double {
        return trackPrice ?? collectionPrice ?? itemPrice ?? 0.0
    }
    
    var artist: String {
        artistName != nil ? String(format: "%@ (%@)", artistName!, type) : "Unknown"
    }
    
    var imageSmallURL: URL? {
        URL(string: imageSmall ?? "")
    }
    
    var imageLargeURL: URL? {
        URL(string: imageLarge ?? "")
    }
    
    var type: String {
        let kind = self.kind ?? "audiobook"
        return typeForKind[kind] ?? kind
    }
    
    var genre: String {
        if let genre = itemGenre {
            return genre
        } else if let genres = bookGenre {
            return genres.joined(separator: ", ")
        }
        return ""
    }
}

extension SearchResult: Comparable {
    static func < (lhs: SearchResult, rhs: SearchResult) -> Bool {
        lhs.name.localizedStandardCompare(rhs.name) == .orderedAscending
    }
    
    static func == (lhs: SearchResult, rhs: SearchResult) -> Bool {
        lhs.name == rhs.name
    }
}




