//
//  SearchResultCell.swift
//  StoreSearch
//
//  Created by Hui Chih Wang on 2020/12/30.
//

import UIKit

class SearchResultCell: UITableViewCell {
    
    var downloadTask: URLSessionDownloadTask?
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var artistNameLabel: UILabel!
    @IBOutlet var artworkImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let selectedView = UIView(frame: CGRect.zero)
        selectedView.backgroundColor = UIColor(named: "SearchBar")?.withAlphaComponent(0.5)
        selectedBackgroundView = selectedView
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        downloadTask?.cancel()
        downloadTask = nil
    }
    
    func configureCell(for result: SearchResult) {
        nameLabel.text = result.name
        
        if result.artist.isEmpty {
          artistNameLabel.text = NSLocalizedString("Unknow", comment: "artist name empty: result cell")
        } else {
          artistNameLabel.text = String(format: NSLocalizedString("ARTIST_NAME_LABEL_FORMAT", comment: "format for artist name"), result.artist, result.type)
        }
        
        artworkImageView.image = UIImage(systemName: "photo")
        
        if let smallUrl = result.imageSmallURL {
            downloadTask = artworkImageView.loadImage(remoteURL: smallUrl)
        }
    }
}
